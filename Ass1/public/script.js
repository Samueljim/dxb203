function searchList() {
    // Declare variables

    var input, filter, ul, li, p, i, card;
    input = document.getElementById('searchBar');
    filter = input.value.toUpperCase();
    ul = document.getElementById("recipes");
    li = ul.getElementsByClassName('noBullet');
    listItem = ul.getElementsByClassName('listItem');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        p = li[i].getElementsByTagName("h3")[0];
        if (p.innerHTML.toUpperCase().indexOf(filter) > -1) {
            listItem[i].style.display = "";
        } else {
            listItem[i].style.display = "none";
        }
    }
};

$(document).ready(function() {
    $('.nav-icon').on('click', function() {
        $('.cross').toggleClass('active');
        $('nav ul').toggleClass('show');
    });



    // Add smooth scrolling to all links
    $("a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function() {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});
